% Problems in programming for research
% Matthew Brett
% August 5th 2016

[//]: # (
A presentation, in .pdf format, describing problems faced in programming for
research and strategies to address them, targeting 10 minutes including
questions
)

# Plan

Problems:

* inexperienced developers writing for specific environments;
* fragile maintenance;
* academic system not designed for developers.

Discussion:

* explaining;
* training.

# Inexperienced developers

* little or no training in coding;
* little value attached to good code;
* researchers often work alone;
* hard to find a mentor.

# Symptoms

* poorly written code;
* few tests;
* poor documentation;
* no search for [ubiquitous
  error](http://blog.nipy.org/ubiquity-of-error.html);
* difficult to track provenance of results.

# Specific environments

[//]: # (
> In the early years of programming, a program was regarded as the private
> property of the programmer.  One would no more think of reading a
> colleague's program unbidden than of picking up a love letter and reading
> it.  This is essentially what a program was, a love letter from the
> programmer to the hardware, full of the intimate details known only to
> partners in an affair.  Consequently, programs became larded with the pet
> names and verbal shorthand so popular with lovers who live in the blissful
> abstraction that assumes that theirs is the only existence in the universe.
> Such programs are unintelligible to those outside the partnership." --
> Michael Marcotty, quoted in "Code complete" by Steven McConnell.
)

Correct code execution may depend on:

* local hardware or software environment;
* expected but undocumented characteristics of data.

# Treatment for bad code, specific environments

* agreed best practice;
* contact with experienced programmers;
* code review, pair coding;
* cross-lab and / or open-source collaboration;
* version control;
* code testing;
* data testing e.g. [testdat](https://github.com/ropensci/testdat);
* continuous integration on multiple platforms;
* deployment with VMs or docker.

# Fragile maintenance

Problem:

* scientific code takes an unusual combination of domain expertise, coding
  experience, and time;
* code often written by PhD students and post-docs;
* scientists often change labs and research area;
* project governance is hard.

# Fragile maintenance

Symptoms:

* large numbers of abandoned scientific code projects;

Treatment:

* concentrate on code quality, tests;
* pay early attention to recruiting developers;
* teach about project life-cycle and open-source governance;
* make plans for succession;
* fund scientists to maintain their code.

[//]: # (get experience of working in other open-source projects.)

# Academic system not designed for developers

Problem:

* learning to be a good developer takes a long time;
* cost of developing, maintaining and releasing code;
* developers usually produce fewer publications and grants;
* contribution to the commons not directly valued.

# Academic system not designed for developers

Symptoms:

* loss of skilled programmers to industry;
* loss of mentors for next generation of scientists;
* wasted effort from loss of code and collaboration.

Treatment:

* specific recognition of code as scientific output;
* add specialized track for scientist-developers, as branch of research
  methods;
* research software engineers and the "body of practice".

# Explaining

We are using the wrong metaphors:

* software "carpentry" - but coding is an intellectual not a practical
  discipline;
* "When I drive a car, I don't need to understand how a car works".

Alternatives:

* proof in mathematics;
* statistics in physical and life sciences.

# Training

* train people in a way that allows them to continue learning;
* model good practice in domain-specific technical teaching;
* be careful of interactive environments.
