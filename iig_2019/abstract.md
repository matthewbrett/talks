# What is the best way to teach imaging analysis?

## Summary

Nearly all of us will know the traditional model for teaching imaging
analysis.  It consists of a series of lectures giving the big picture,
combined with workshops using specialized software such as SPM or FSL. I've
taught that way myself, for many years, but it gradually became clear to me
that I was leaving my students with very superficial understanding. Meanwhile,
my work in open source software was teaching me more efficient and
reproducible practice for analysis, but I was not passing this on in my
teaching.

One might defend the traditional model by asserting that we don't have time to
teach students to program, to understand the basic ideas, and to use efficient
practice for computation.  But - is that true?

We tested that defense by running courses on basic brain imaging that did
teach programming and fundamental ideas from the linear model, as well as
efficient tools and practice for reproducibility [1, 2].   In general the
courses worked very well [1]. I believe they should inform a new standard
model of teaching in brain imaging, and other fields with high demands for
cross-disciplinary technical work, and the pervasive use of computing.

[1] https://www.frontiersin.org/articles/10.3389/fnins.2018.00727/full
[2] https://bic-berkeley.github.io/psych-214-fall-2016/classes_and_labs.html
