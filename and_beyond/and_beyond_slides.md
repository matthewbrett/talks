% Teaching with the IPython notebook
% Matthew Brett and your noble selves
% May 13 2015

# Why?

The joys and sorrows of a life spent with the IPython notebook

# Whence?

* Teaching brain imaging;
* The relationship of ideas and code and algorithms;

# The immediate inspiration

http://blog.yhathq.com/posts/introducing-rodeo.html

# How?

Use-cases

# The course

http://practical-neuroimaging.github.io/

* Prior reading / homework for each week of approx 30 minutes;

* Class is 2 hours:

    * 10 minutes debrief from previous class
    * 30 minutes talk introduction + questions;
    * 60 minutes problems;
    * 10 minutes review of problems;

# Use-cases for teaching

* Making tutorials for reading;
* Introduction to Python / programming topics;
* Exercises;
* (Grading).

# Other use-cases I won't talk about

* Throwing around ideas, including commentary;
* Executable documents for presenting research;
* Slides for stand-up talks;
* As an IDE.

# Where the notebook shines

* Introduction to programming topics (shift-enter-along-with-me);
* Exercises.

# Where the notebook nearly shines

Tutorials

* Curious coder's guide to git;
* Convolution;

# The problem

* Does not match the way I usually write;
* Makes it harder for me to think;
* Documents are difficult to edit, and to rearrange, to version, to
  collaborate on;
* Hard to integrate into websites;

# The current alternatives

* Sphinx with custom extensions;
* Keynote (yes, seriously);
* LaTeX and PDF?
* Dexy?

# Where we need to go

* You tell me
