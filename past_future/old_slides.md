
# The scientific ideal

> Science alone of all the subjects contains within itself the lesson of the
> danger of belief in the infallibility of the greatest teachers in the
> preceding generation... Learn from science that you must doubt the experts
> ...
> Science is the belief in the ignorance of experts

Richard Feynman, What is Science? (1969)

# Take no-one's word for it

\centerline{\includegraphics[height=2.5in]{nullius_in_verba.jpg}}

(by kladcat under [CC BY 2.0](http://creativecommons.org/licenses/by/2.0), via
Wikimedia Commons)

# Ubiquity of error

> The scientific method's central motivation is the ubiquity of error - the
> awareness that mistakes and self-delusion can creep in absolutely anywhere
> and that the scientist's effort is primarily expended in recognizing and
> rooting out error."

Donoho, David L, et al. 2009. Reproducible research in computational
harmonic analysis. *Computing in Science & Engineering* 11, 8--18.

# Understanding by building

> What I cannot create, I do not understand

Found on Richard Feynman's blackboard at the time of his death.

# The dreadful continuum

\centerline{\includegraphics[width=4in]{continuum.png}}

# Culture is everything

> Until I came to IBM, I probably would have told you that culture was just
> one among several important elements in any organization's makeup and
> success - along with vision, strategy, marketing, financials, and the
> like... I came to see, in my time at IBM, that culture isn't just one aspect
> of the game, it is the game.

Lou Gerstner "Who says elephants can't dance"

> The rewards system is a powerful driver of behavior and therefore culture.

# Scientific culture

> The scientific method's central motivation is the ubiquity of error.

> Science is the belief in the ignorance of experts.

> What I cannot create, I do not understand.

# The Raspberry Pi story

\centerline{\includegraphics[height=2.5in]{Raspberry_Pi_-_Model_A.jpg}}

By SparkFun Electronics from Boulder, USA - Raspberry Pi - Model A, CC BY 2.0,
https://commons.wikimedia.org/w/index.php?curid=26785859
