Improving imaging research

There is a wide gap between the ideal and the reality of scientific culture in
neuroimaging.  This culture gap is one reason of many to suggest that a large
proportion of published imaging studies are wrong.  Reasons for the culture
gap include the perverse nature of academic rewards, the lack of systematic
education in the foundations of imaging, and the complexity of imaging
software.

I will talk about work we have been doing in Berkeley to redesign education in
neuroimaging with an emphasis on algorithmic thinking, sound techniques for
organizing code and data, and mathematical principles of the analysis.  Our
courses finish with projects that must be reproducible from downloading the
data to the figures in the report.  I will also discuss the problem of culture
in neuroimaging software, and argue that we can do more to give our students
ownership of their analysis.
