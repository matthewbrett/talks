% Making research more true
% Matthew Brett
% November 21, 2014

# Cutting salt intake is good for you health

- Cutting salt intake reduces blood pressure;
- Reducing high blood pressure with drugs reduces risk of heart attacks and
  strokes.

- Therefore - we should cut salt intake according to:
    - American Heart Association
    - American Medical Association
    - American Public Health Association

Michael Alderman JAMA. 2010;303(5):448-449. doi:10.1001/jama.2010.69

# The drunk and the lamp post

# NIMBY

Suppose that the National Co- operative for the Storage of Nuclear Waste
(NAGRA), after completng exploratory drilling, proposes to build the
repository for low- and mid-level radioactive waste in your hometown.  Federal
experts examine this proposition, and the federal parliament decides to build
the repository in your community. In a townhall meeting, do you accept this
proposition or do you reject this proposition?

# Fixing NIMBY

.. Moreover, the parliament decides to compensate all residents of the host
community with 5,000 francs per year and per person. Your family will thus
receive xxx francs per year.  The compensation is financed by all taxpayers in
Switzerland.  ...

- $2200 / individual / year
- $4300
- $6600

# Fixing NIMBY backfire

- No compensation - 50.8% acceptance
- Compensation - 24.6% acceptance

BS Frey, F Oberholzer-Gee (1997) The American economic review

# Encouraging parents to pick up their kids

Private day-care center, parents late to collect children.

- 4 weeks baseline
- Fine of 10 NIS per child collected more than 10 minutes late
- Fine stopped beginning of week 17

# Paying parents to pick up their kids

\centerline{\includegraphics[width=5in]{gneezy_data.png}}

U Gneezy, A Rustichini (2000) J. Legal Studies

# Creative problem solving

\centerline{\includegraphics[height=3in]{duncker_candle.jpg}}

K Duncker (1945) Psychological Monographs, 58

# Improved by rewards

- **Low-drive** - We are doing pilot work on various problems in order to decide
  which will be the best ones to use in an experiment we plan to do later. We
  would like to obtain norms on the time needed to solve
- **High-drive** - Depending on how quickly you solve the problem you can win
  $5.00 or $20.00. The top 25% of the Ss [subjects] in your group will win
  $5.00 each; the best will receive $20.00. Time to solve will be the
  criterion used. ($39 and $156 at today's rates)
- *Dm+* - solution is "dominant": the easy task, pins outside the box;
- *Dm-* - solution is not "dominant": the hard task, pins inside the box;

# Yes, if the task is very easy

\centerline{\includegraphics[height=3in]{glucksberg_table1.png}}

S Glucksberg (1962) J Exp. Psychol

# Rewards and teaching

"5th and 6th graders acted as tutors for the 1st and 2nd graders. In the
reward condition, 12 6th-grade girls were promised movie tickets for
successfully teaching a sorting game to a 1st-grade girl. In the no-reward
condition, Ss taught the game without promise of reward. Ss were randomly
assigned to pairs and conditions. Results indicate more criticism, more
demands, and less efficacious use of time in the reward condition. The
no-reward condition was marked by a more positive emotional tone, greater
learning by younger Ss, and fewer errors."

Gabarino (1975) J Personality Social Psychology

# Motivating with rewards

".. compared to non-rewarded subjects, subjects offered a task- extrinsic
incentive choose easier tasks, are less efficient in using the information
available to solve novel problems, and tend to be answer oriented and more
illogical in their problem-solving strategies. They seem to work harder and
produce more activity, but the activity is of lower quality, contains more
errors, and is more stereotyped and less creative than the work of comparable
nonrewarded subjects working on the same problems"

J Condry (1977) Journal of Personality and Social Psychology (quoted in
"Punished by reward by Alfie Kohn).

# Punished by rewards

\centerline{\includegraphics[height=3in]{punished_rewards.jpg}}

# Cheating and rewards

\centerline{\includegraphics[width=5in]{anderman_questions.png}}

# Cheating and rewards

\centerline{\includegraphics[width=5in]{anderman_table1.png}}

Anderman et al (1998) J Educational Psychology

# Cheating and participation

Swiss cantons with referendum compared to representative democracies have
lower levels of tax avoidance

Pommerehne and Frey (1993)
http://econstor.eu/bitstream/10419/101488/1/746529821.pdf

# The paper - box 1

\centerline{\includegraphics[width=5in]{ioannidis_box1.png}}

# The paper - table 2

\centerline{\includegraphics[width=5in]{ioannidis_table2.png}}
