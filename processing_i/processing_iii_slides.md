% Data processing III
% Matthew Brett
% November 3, 2014

# Subject 9 from ds114

\centerline{\includegraphics[height=3in]{sub009_rendered.png}}

# Colin Holmes average

\centerline{\includegraphics[height=2.8in]{colin_rendered.png}}

> http://www.mccauslandcenter.sc.edu/mricro/mricron/

# Cingulate cortex

\centerline{\includegraphics[width=5in]{cingulate_extent.png}}

> Pujol et al (2002) NeuroImage 15:847

#

\centerline{\includegraphics[width=4.5in]{cingulate_config.png}}

# Broca

\centerline{\includegraphics[height=3in]{broca_variable.png}}

> Amunts et al (1999) J Comp Neurol 412:319

# Affine transformations

\centerline{\includegraphics[width=4.5in]{affine_transforms.png}}

# Bad transforms

\centerline{\includegraphics[height=3in]{crazy_warping.png}}

# Volumes and surfaces

\centerline{\includegraphics[height=2.75in]{vol_surf_compare.png}}

> Fischl et al (1999) Hum Brain Mapp 8:272
