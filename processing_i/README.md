# Running the IPython notebooks

The `.ipynb` files in this directory are [IPython notebooks](http://ipython.org/notebook.html).

These are executable documents; you can execute the code in them and try
different parameters and code of your own.

If you would like to do this, I've prepared a
[Virtualbox](https://www.virtualbox.org/) image so you can load up the the
notebooks with all the software you need.

First, download the virtual image via [this
link](https://nipy.bic.berkeley.edu/practical_neuroimaging/fmri_processing_notebooks.ova).
It's a 2.8GB file, so you might want to wait until you have a good connection.

Then follow the instructions on [installing the NeuroDebian virtual
machine](http://neuro.debian.net/vm.html) except using the
`fmri_processing_notebooks.ova` file instead of the NeuroDebian virtual
machine file.

When you start the virtual machine, you should see an icon on the desktop
called "Start notebooks".  Double click on that and wait 20 seconds or so for
the notebook server to run.  Now you can click on the individual notebooks and
run the code in them.

As always, profound thanks to the
[NeuroDebian](http://neuro.debian.net/index.html) team for making this
possible.
