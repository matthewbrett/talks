% Data processing II
% Matthew Brett
% October 27, 2014

# Diagnostics by finding components

Finding sets of voxels that covary together over time.

Some of the reasons voxels vary together is brain activation.

Usually image artefacts are a much bigger reason for voxel covariance

# Principal components analysis (PCA)

See the `diagnostics` and `diagnostics_again` notebooks.

For background on PCA see:

- A tutorial on PCA at http://arxiv.org/abs/1404.1100 (needs some basic linear
  algebra)
- A more basic tutorial on PCA at
  http://www.cs.otago.ac.nz/cosc453/student_tutorials/principal_components.pdf

# Independent components analysis (ICA)

Implemented by `MELODIC` in FSL:

- FSL: http://fsl.fmrib.ox.ac.uk/fsl/fslwiki
- MELODIC: http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/MELODIC

See the `melodic` command at the end of the `diagnostics` notebook.

# Simple diagnostics

Simple diagnostics

- Mean, min, max standard deviation across time for each voxel;
- Mean across all voxels for each volume;
- Subtracting each volume from the previous volume to look for large
  differences;

See:

- http://imaging.mrc-cbu.cam.ac.uk/imaging/DataDiagnostics
- Data diagnostic tools in http://nipy.org/nipy
- The `diagnostics_again` notebook

# Slice timing correction

The problem of slices acquired at different times.

Solved by *interpolation* in one dimension.

See `slice_timing` notebook and:

- http://en.wikipedia.org/wiki/Linear_interpolation
- http://en.wikipedia.org/wiki/Interpolation

# Motion correction

Subjects move over the course of a scan

Movement causes large signal changes

Correcting movement consists of:

- Estimating how much each scan has moved relative to a standard such as the
  first volume in the time-series;
- Resampling the scans to correct for this movement

# Estimating movement between scans

Calculating the movement consists of:

- Choosing a metric for how well two images are matched (the *cost function*);
- Moving the image relative to the reference until this metric shows a best
  match (*optimizing* the cost function for movement parameters).

See the `optimizing_space` notebook.

# Resampling

After we have estimated movement between - say - the first scan and the second
scan, we need to make a new copy of the second scan where the scan has moved
to match the first scan.

We do this by using the estimated motion parameters.

- Make an empty volume the same shape as the first volume
- For each voxel in this empty volume;
    - use the movement parameters to work out the corresponding voxel
      coordinate in the second volume.
    - Fill the voxel with an estimate (by resampling) of the voxel value for
      the voxel coordinate in the second volume

- http://en.wikipedia.org/wiki/Bilinear_interpolation
