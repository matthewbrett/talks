##############################
Sample subject data from ds114
##############################

This directory contains one FMRI run and an anatomical scan from one subject
in the ds114 dataset of OpenFMRI.

* OpenFMRI: https://openfmri.org
* ds114: https://openfmri.org/dataset/ds000114

The data is unmodified from the data available in the ``ds114_raw.tar.gz``
archive linked from the ds114 page above.

ds114 carries a `PDDL <http://opendatacommons.org/licenses/pddl/1.0>`_
license.
