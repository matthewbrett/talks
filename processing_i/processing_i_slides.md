% Data processing I
% Matthew Brett
% October 20, 2014

# A history of bioinformatics (in the year 2039)

https://www.youtube.com/watch?v=uwsjwMO-TEA

# Lots and lots of mistakes

\centerline{\includegraphics[width=5in]{begley_ellis.png}}

# It does not come out in the wash

\centerline{\includegraphics[width=5in]{begley_citations.png}}

# Ubiquity of error

\centerline{\includegraphics[width=5in]{donoho_invitation.png}}

    David Donoho - An invitation to reproducible computational research (2010)

# Selling the story

\centerline{\includegraphics[height=3in]{donoho_advertising.png}}

# Disbelief

Richard Feynman, What is Science? (1969)

> Science alone of all the subjects contains within itself the lesson of the
> danger of belief in the infallibility of the greatest teachers in the
> preceding generation... Learn from science that you must doubt the experts
> ...
> Science is the belief in the ignorance of experts

# Conversation

A scientist:

> I analyzed these data with *my favorite software*; the analysis showed
> activation in the frontal lobe.

Another scientist:

> I don't believe you.  Show me what you did in detail so I can check if you
> made a mistake.

# For this lifetime, you will need a

- programming language
- plan of how you will work

# Programming language

\centerline{\includegraphics[width=5in]{teach_yourself.png}}

# How to work

\centerline{\includegraphics[width=5in]{best_practices.png}}

# Creating and understanding

\centerline{\includegraphics[height=3in]{feynman.jpg}}

# What is an image?

\centerline{\includegraphics[height=3in]{small_school.png}}

# Example brain images

- https://openfmri.org/dataset/ds000114
- http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3641991/

# Tools

- Python
- nipy / nibabel (http://nipy.org)
- FSL (http://fsl.fmrib.ox.ac.uk/fsl/fslwiki)

