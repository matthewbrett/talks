% How do we teach people to use good text editors?
% Matthew Brett
% February 11 2015

# Pragmatic programmer

*Use a Single Editor Well*

The editor should be an extension of your hand; make sure your editor is
configurable, extensible, and programmable.

"The Pragmatic programmer: from journeyman to master" by Andrew Hunt, David Thomas

# Do modern programmers use emacs?

"I do think that to a lot of younger programmers it probably looks arcane
(an elegant weapon for a more civilised age) and the need to learn
keystrokes pretty much from the start is probably offputting to someone
who doesn't really want to master it, although the benefits are
remarkable.

"It seems likely to me that tools like that are unlikely to appeal to
younger programmers because they don't have the experience to appreciate
the benefits of a truly powerful text editor. Coming to appreciate that
is, perhaps, a sign of maturity in itself."

http://programmers.stackexchange.com/questions/86659/does-new-generation-of-programmers-use-emacs

# Ditto, about vim

"Everyone holds their breath in awe when I change 20 lines of code in three seconds...

"Productivity is key, especially when you have tight schedules and need time
to think about the solution. You just can’t waste your time by spending most
of it editing the code.

"You’re being paid for thinking. So use a tool that lets you think more and
lets you flesh out your ideas quicker."

http://programmers.stackexchange.com/questions/86659/does-new-generation-of-programmers-use-emacs

# But I am not a programmer

* I don't write code, I write - er - scripts;
* I just need something that will get the job done, I don't want to invest
  time in learning something that is only for experts;
* I'm not interested in the cutting edge, good enough is good enough for me.

What is the cost to a scientist of being a bad programmer?

# What does persuade?

Maybe:

* Taking it on faith (from who?);
* Watching other people do stuff;
* Increased efficiency *of thought*.

# We are bad at thinking two things

"This study assessed the effects of vocal music, equivalent instrumental
music, and irrelevant speech on WM in order to clarify what aspect of music
affects performance and the degree of impairment. ... both speech and vocal
music degraded performance ... People were poor judges of the degree of memory
impairment resulting from various irrelevant sounds."

The Relative and Perceived Impact of Irrelevant Speech, Vocal Music and
Non-vocal Music on Working Memory, Curr Psychol (2008) 27:277–289

# Thinking two things

\centerline{\includegraphics[width=5in]{speech_wm.png}}

# Intention to treat

* 1000 students;
* 6 week course;
* 500 taught on vim or emacs;
* 500 taught on text editor of student's initial choice;
* Rate for efficiency after 1 year

# How can we teach?

* Testimony;
* Show good editing in use;
* Compare good and bad text editors in use;
* Editing competitions or games?
