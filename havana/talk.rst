=============================================
The need and methods for reproducible science
=============================================

.. |bullet| unicode:: U+02022
.. |emdash| unicode:: U+02014

The state of the field
----------------------

What kind of field are we in?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Simplicity
==========

.. epigraph::

    Since the Romans have taught us "Simplex Veri Sigillum" —that is: simplicity
    is the hallmark of truth— we should know better, but complexity continues to
    have a morbid attraction. When you give for an academic audience a lecture
    that is crystal clear from alpha to omega, your audience feels cheated and
    leaves the lecture hall commenting to each other: "That was rather trivial,
    wasn't it?" The sore truth is that complexity sells better. (It is not only
    the computer industry that has discovered that.)

       |emdash| Edsger Dijkstra "The threats to computing science"
       http://www.cs.utexas.edu/users/EWD/transcriptions/EWD08xx/EWD898.html

Replication and reproducibility
===============================

* Replication : if I take an experiment, repeat it, and find substantially the
  same results, I have *replicated* the experiment

* Reproducibility : if I take the *original* data and code from an experiment,
  and reproduce the main results, I have *reproduced* the analysis.

Replication
===========

.. epigraph::

    Let us say you took a random sample of papers using functional MRI over the
    last five years. For each study in the sample, you repeated the same
    experiment.  What proportion of your repeat experiments would substantially
    replicate the main findings of the original paper

Replication in hematological oncology
=====================================

Scientists at Amgen (a drug company) tried to reproduce findings from 53
"landmark" studies.

.. epigraph::

    ... when findings could not be reproduced, an attempt was made to contact
    the original authors, discuss the discrep- ant findings, exchange reagents
    and repeat experiments under the authors’ direction, occasionally even in
    the laboratory of the original investigator.

Glenn Begley and Lee Ellis "Raise standards for preclinical cancer research"
Nature 483 (2012)

Hematological oncology replication rate
=======================================

Of 53 studies, only 6 replicated (11%).

.. image:: ../pics/begley_table.pdf
   :width: 80%
   :align: center

Hematological oncology replication rate
=======================================

.. epigraph::

    In studies for which findings could be reproduced, authors had paid close
    attention to controls, reagents, investigator bias and describing the
    complete data set. For results that could not be reproduced, however, data
    were not routinely analysed by investigators blinded to the experimental
    versus control groups. Investigators frequently presented the results of one
    experiment, such as a sin- gle Western-blot analysis. They sometimes said
    they presented specific experiments that supported their underlying
    hypothesis, but that were not reflective of the entire data set.

Most research findings are false
================================

.. image:: ../pics/ioannidis_title.png
   :width: 80%
   :align: center

More in the notebook

Power
=====

.. image:: ../pics/power_distribution.pdf
   :width: 80%
   :align: center

When most research findings are false 1
=======================================

.. image:: ../pics/ioan_cor1.png
   :width: 80%
   :align: center

When most research findings are false 2
=======================================

.. image:: ../pics/ioan_cor2.png
   :width: 80%
   :align: center

When most research findings are false 3
=======================================

.. image:: ../pics/ioan_cor3.png
   :width: 80%
   :align: center

When most research findings are false 4
=======================================

.. image:: ../pics/ioan_cor4.png
   :width: 80%
   :align: center

When most research findings are false 5
=======================================

.. image:: ../pics/ioan_cor5.png
   :width: 80%
   :align: center

When most research findings are false 6
=======================================

.. image:: ../pics/ioan_cor6.png
   :width: 80%
   :align: center

So what is our study power?
===========================

In neuroimaging studies of brain volume abnormalities:

.. epigraph::

    Our results indicated that the median statistical power of these studies was
    8% across 461 indi- vidual studies contributing to 41 separate
    meta-analyses, which were drawn from eight articles that were published
    between 2006 and 2009.

      |emdash| Power failure: why small sample size undermines the reliability
      of neuroscience by Katherine S. Button, John P. A. Ioannidis, Claire
      Mokrysz, Brian A. Nosek, Jonathan Flint, Emma S. J. Robinson and Marcus R.
      Munafò (2013)

How about bias?
===============

Bias is difficult to estimate.

Clear 'excess significance bias' in brain volume abnormality studies'

See:

Ioannidis, J. P. Excess significance bias in the literature on brain volume
abnormalities. Arch. Gen. Psychiatry 68, 773–780 (2011).

Are we really doing science or something else?
==============================================

* deep magic - based on esoteric theoretical knowledge
* black magic - based on techniques without theoretical explanation
* heavy wizadry - based on obscure/undocumented inticracies of specific
  harware/software
* cargo cult programming
* shotgun debugging
* vodoo programming -  The use by guess or cookbook of an obscure or hairy
  system, feature, or algorithm that one does not truly understand.
* rain dance / waving the dead chicken - ceremonial action taken to correct a
  hardware problem

Reproducibility
===============

Can we or others reproduce our results from our own data?

The crisis in computational science
===================================

.. epigraph::

    In my own experience, error is ubiquitous in scientific computing, and one
    needs to work very diligently and energetically to eliminate it. One needs a
    very clear idea of what has been done in order to know where to look for
    likely sources of error. I often cannot really be sure what a student or
    colleague has done from his/her own presentation, and in fact often his/her
    description does not agree with my own understanding of what has been done,
    once I look carefully at the scripts. Actually, I find that researchers
    quite generally forget what they have done and misrepresent their
    computations.

      |emdash| David L. Donoho (2010). An invitation to reproducible
      computational research. Biostatistics Volume 11, Issue 3 Pp. 385-388

The crisis in computational science
===================================

.. epigraph::

    Computing results are now being presented in a very loose, “breezy” way—in
    journal articles, in conferences, and in books. All too often one simply
    takes computations at face value. This is spectacularly against the evidence
    of my own experience. I would much rather that at talks and in referee
    reports, the possibility of such error were seriously examined.

      |emdash| David L. Donoho (2010). An invitation to reproducible
      computational research. Biostatistics Volume 11, Issue 3 Pp. 385-388

Climategate
===========

::

    READ ME for Harry's work on the CRU TS2.1/3.0 datasets, 2006-2009!

    1. Two main filesystems relevant to the work:

    /cru/dpe1a/f014
    /cru/tyn1/f014

    Both systems copied in their entirety to /cru/cruts/

    Nearly 11,000 files! And about a dozen assorted 'read me' files addressing
    individual issues, the most useful being:

    fromdpe1a/data/stnmon/doc/oldmethod/f90_READ_ME.txt
    fromdpe1a/code/linux/cruts/_READ_ME.txt
    fromdpe1a/code/idl/pro/README_GRIDDING.txt

    (yes, they all have different name formats, and yes, one does begin '_'!)


Error in computational science
==============================

.. epigraph::

    In stark contrast to the sciences relying on deduction or empiricism,
    computational science is far less visibly concerned with the ubiquity of
    error. At conferences and in publications, it’s now completely acceptable
    for a researcher to simply say, “here is what I did, and here are my
    results.” Presenters devote almost no time to explaining why the audience
    should believe that they found and corrected errors in their computations.
    The presentation’s core isn’t about the struggle to root out error — as it
    would be in mature fields — but is instead a sales pitch: an enthusiastic
    presentation of ideas and a breezy demo of an implementation.

        (continued)

Error in computational science
==============================

.. epigraph::

    Computational science has nothing like the elaborate mechanisms of formal
    proof in mathematics or meta-analysis in empirical science.  Many users of
    scientific computing aren’t even trying to follow a systematic, rigorous
    discipline that would in principle allow others to verify the claims they
    make. How dare we imagine that computational science, as routinely
    practiced, is reliable!

       |emdash| David L. Donoho, Arian Maleki, Inam Ur Rahman, Morteza Shahram
       and Victoria Stodden (2009) Reproducible Research in Computational
       Harmonic > Analysis.  Computing in Science and Engineering 11(1) pp 8-18

The answer is...
~~~~~~~~~~~~~~~~

"Take nobody's word for it"
===========================

.. image:: ../pics/nullius1.png
   :width: 50%
   :align: center

"Science is the belief in the ignorance of experts"
===================================================

.. epigraph::

  Science alone of all the subjects contains within itself the lesson of the
  danger of belief in the infallibility of the greatest teachers in the
  preceding generation... Learn from science that you must doubt the experts.

    |emdash| Richard Feynman, What is Science? (1969)

"... none **absolutely** certain"
===============================================================

.. epigraph::

  When a scientist doesn't know the answer to a problem, he is ignorant. When
  he has a hunch as to what the result is, he is uncertain. And when he is
  pretty darned sure of what the result is going to be, he is in some doubt...
  Scientific knowledge is a body of statements of varying degrees of certainty
  |emdash| some most unsure, some nearly sure, none **absolutely** certain.  

    |emdash| Richard Feynman, What Do You Care What Other People Think? (1988)

The importance of error
=======================

.. image:: ../pics/donoho_error.png
   :width: 80%
   :align: center

"...truth will sooner come out of error than from confusion."
=============================================================

.. epigraph::

  ...so when a man tries all kinds of experiments without method or
  order, this is mere groping in the dark; but when he proceeds with
  some direction and order in his experiments, it is as if he were
  led by the hand...

    |emdash| Francis Bacon, Novum Organum (1620)

What is a paper?
================

.. image:: ../pics/donoho_advertising.png
   :width: 80%
   :align: center

What should we publish?
=======================

.. epigraph::

  In summary, the idea is to try to give all of the information to
  help others to judge the value of your contribution; not just the
  information that leads to judgment in one particular direction or
  another.

    |emdash| Richard Feynman, Cargo Cult Science (1974)

A current research effort
=========================

.. image:: ../pics/gentleman2007.pdf
   :width: 95%
   :align: center

How do we move forward?
-----------------------

What do we need?
~~~~~~~~~~~~~~~~

What do we need?
================

* A deep change in culture
* Take no-one's word for it
* Skepticism comes from understanding
* Trust comes from transparency
* Neuroimaging is a computational science
* We need to train the next generation in computational methods

We your teachers are the problem
================================

.. image:: ../pics/darth.png
   :width: 95%
   :align: center

How do we train the next generation of scientists?
==================================================

* scientific practice
* computational literacy
* software engineering
* repdroducible research

Tools for reproducible research
===============================

.. epigraph::

    That which I cannot create, I do not understand

       |emdash| Richard Feynman or possibly Nelson Trujillo

Tools we use now
================

* Training in "software carpentry"
* Scripting languages which are easy to read and write
* Code review across the research group
* Version control and releases for papers
* The practice of autjomated testing
* Data organization
* Vigorous tranparency
