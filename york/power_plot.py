""" Make power plots
"""

import numpy as np
np.set_printoptions(precision=4)  # print arrays to 4 decimal places
import matplotlib.pyplot as plt

import scipy.stats

high_var = 0.5
effect = 1.
thresh = 0.05
low_var = 3

# Number of variance units left and right of center on plot
lim_vars = 4

high_lims = [lim_vars * -high_var, lim_vars * high_var + effect]
low_lims = [lim_vars * -low_var, lim_vars * low_var + effect]
x = np.linspace(low_lims[0], low_lims[1], 1000)

z_dist_1 = scipy.stats.norm(scale=high_var)
z_dist_1_h1 = scipy.stats.norm(scale=high_var, loc=effect)
z_pdf_1 = z_dist_1.pdf(x)
z_pdf_1 /= z_pdf_1.max()
z_dist_2 = scipy.stats.norm(scale=low_var)
z_dist_2_h1 = scipy.stats.norm(loc=effect, scale=low_var)
z_pdf_2 = z_dist_2.pdf(x)
z_pdf_2 /= z_pdf_2.max()
h1_x = x + effect
z_1_thresh = z_dist_1.ppf(1 - thresh)
z_2_thresh = z_dist_2.ppf(1 - thresh)

h0_color = (0, 0, 0, 0.3)  # Light gray in RGBA format.
h1_color = (0, 0, 0, 0.2)  # Light gray in RGBA format.

fig, (high, low) = plt.subplots(2, 1)

pp_fmt = '$1 - \\beta={:.2f}$\n$Pr(H_A | T+)={:.2f}$'

high.plot(x, z_pdf_1, label='$H_0$')
high.plot([0, 0], [0, 1], 'g:')
high.plot(x + effect, z_pdf_1, label='$H_A$')
high.plot([effect, effect], [0, 1], 'g:')
high.fill_between(x, z_pdf_1,
                  where=x >= z_1_thresh,
                  color=h0_color)
high.fill_between(h1_x, z_pdf_1,
                  where=h1_x >= z_1_thresh,
                  color=h1_color)
power_1 = 1 - z_dist_1_h1.cdf(z_1_thresh)
pp_1 = power_1 / (power_1 + thresh)
high.text(high_lims[1] * 0.6, 0.50, pp_fmt.format(power_1, pp_1))

high.axis(high_lims + [0, 1.2])
high.legend()
low.plot(x, z_pdf_2, 'b', label='$H_0$')
low.plot([0, 0], [0, 1], 'b:')
low.plot(x + effect, z_pdf_2, 'g', label='$H_A$')
low.plot([effect, effect], [0, 1], 'g:')
low.fill_between(x, z_pdf_2,
                 where=x >= z_2_thresh,
                 color=h0_color)
low.fill_between(h1_x, z_pdf_2,
                 where=h1_x >= z_2_thresh,
                 color=h1_color)
power_2 = 1 - z_dist_2_h1.cdf(z_2_thresh)
pp_2 = power_2 / (power_2 + thresh)
low.text(low_lims[1] * 0.5, 0.50, pp_fmt.format(power_2, pp_2))
low.axis(low_lims + [0, 1.2])
low.legend()

plt.savefig('power_difference.png')
